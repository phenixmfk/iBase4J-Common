/**
 *
 */
package top.ibase4j.core.support.context;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.ReferenceConfig;
import com.alibaba.dubbo.config.RegistryConfig;
import com.weibo.api.motan.config.ProtocolConfig;
import com.weibo.api.motan.config.RefererConfig;

import top.ibase4j.core.base.AbstractController;
import top.ibase4j.core.config.RpcConfig;
import top.ibase4j.core.util.InstanceUtil;

/**
 *
 * @author ShenHuaJie
 * @version 2017年12月6日 上午11:53:31
 */
@Component
public class ApplicationContextHolder implements ApplicationContextAware {
    private static final Logger logger = LogManager.getLogger();
    static ApplicationContext applicationContext;
    private static final Map<String, Object> serviceFactory = InstanceUtil.newHashMap();
    private static final RpcConfig.EnableDubboReference enableDubbo = new RpcConfig.EnableDubboReference();
    private static final RpcConfig.EnableMotan enableMotan = new RpcConfig.EnableMotan();
    private static ApplicationConfig dubboApplication;
    private static RegistryConfig dubboRegistry;
    private static ProtocolConfig motanProtocol;
    private static com.weibo.api.motan.config.RegistryConfig motanRegistry;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ApplicationContextHolder.applicationContext = applicationContext;
    }

    public static <T> T getBean(Class<T> t) {
        return applicationContext.getBean(t);
    }

    public static <T> Map<String, T> getBeansOfType(Class<T> t) {
        return applicationContext.getBeansOfType(t);
    }

    public static Object getBean(String name) {
        return applicationContext.getBean(name);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public static <T> T getService(Class<T> cls) {
        String clsName = cls.getName();
        T v = (T)serviceFactory.get(clsName);
        if (v == null) {
            synchronized (clsName) {
                v = (T)serviceFactory.get(clsName);
                if (v == null) {
                    logger.info("*****Autowire {}*****", cls);
                    if (enableDubbo.matches(null, null)) {
                        if (dubboApplication == null || dubboRegistry == null) {
                            synchronized (AbstractController.class) {
                                if (dubboApplication == null) {
                                    dubboApplication = ApplicationContextHolder.getBean(ApplicationConfig.class);
                                }
                                if (dubboRegistry == null) {
                                    dubboRegistry = ApplicationContextHolder.getBean(RegistryConfig.class);
                                }
                            }
                        }
                        // 引用远程服务
                        ReferenceConfig<?> reference = new ReferenceConfig<>();
                        reference.setApplication(dubboApplication);
                        reference.setRegistry(dubboRegistry); // 多个注册中心可以用setRegistries()
                        reference.setCheck(false);
                        reference.setInterface(cls);
                        v = (T)reference.get();
                    } else if (enableMotan.matches(null, null)) {
                        if (motanProtocol == null || motanRegistry == null) {
                            synchronized (AbstractController.class) {
                                if (motanProtocol == null) {
                                    motanProtocol = ApplicationContextHolder.getBean(ProtocolConfig.class);
                                }
                                if (motanRegistry == null) {
                                    motanRegistry = ApplicationContextHolder
                                            .getBean(com.weibo.api.motan.config.RegistryConfig.class);
                                }
                            }
                        }
                        // 引用远程服务
                        RefererConfig reference = new RefererConfig<>();
                        reference.setProtocol(motanProtocol);
                        reference.setRegistry(motanRegistry); // 多个注册中心可以用setRegistries()
                        reference.setInterface(cls);
                        reference.setCheck("false");
                        v = (T)reference.getRef();
                    } else {
                        v = ApplicationContextHolder.getBean(cls);
                    }
                    logger.info("*****{} Autowired*****", cls);
                    serviceFactory.put(clsName, v);
                }
            }
        }
        return v;
    }
}
